provider "aws" {
  region = "us-east-1"
}

resource "aws_instance" "puppetagent" {
  ami             = "ami-07d9b9ddc6cd8dd30"
  instance_type   = "t2.micro"
  key_name        = "vicky"
  security_groups = ["puppet"] # Specify your security group here

  tags = {
    Name = "puppetagent"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("vicky.pem")
    host        = self.public_ip
    timeout     = "20m" # Adjust timeout as per your requirement
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo wget https://apt.puppetlabs.com/puppet8-release-bionic.deb",
      "sudo dpkg -i puppet8-release-bionic.deb",
      "sudo apt update",
      "sudo apt install puppet-agent -y",
      "sudo sh -c 'echo \"172.31.37.135 puppet\" >> /etc/hosts'",
      "sudo systemctl enable puppet",
      "sudo systemctl start puppet",
      "sudo systemctl status puppet",
      "sudo /opt/puppetlabs/bin/puppet agent --test"
    ]
  }
}

output "public_ip" {
  value = aws_instance.puppetagent.public_ip
}