Setting up puppet master :
1. Create an ec2 Instance in aws and name it as puppetmaster.
2. Configure the hosts file in puppet master by add the private ip of puppet master instance.
3. Download puppet using wget or curl.
4. Install puppetserver in master instance.
5. start and enable puppet server and check the status of the puppetserver.
- 	sudo systemctl enable puppetserver
- 	sudo systemctl start puppetserver
-	sudo systemctl status puppetserver
 
Setting up puppet agent using terraform :
Sterps to execute terraform : 
1. initialize terraform with terraform init
2. validate the terraform code with terraform validate
3. plan the terraform with terraform plan
4. execute the terraform with terraform apply
 
